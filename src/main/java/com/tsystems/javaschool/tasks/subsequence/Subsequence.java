package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
       try
        {    
        if (x.equals(y))
                return true;
        if (!x.isEmpty() && !y.isEmpty())
        {
            int indexX=0, indexY=0;
            while (x.size()<y.size())
            {
                    if( x.get(indexX)!=y.get(indexY))
                       y.remove(indexY);
                    else
                    {
                        if (indexX+1<x.size())
                            indexX++;
                        indexY++;
                    } 
            }
            if (x.equals(y))
                return true;
        }
        else
        {
            if (x.isEmpty() && !y.isEmpty())
                return true;
        }
        }
        catch (Exception ex)
        {   
            if (x==null && y==null)
                return true;
        }
        return false;
    }
}
